# Advent of Code 2018

Solving AoC problems to get some Ruby under my fingers.

https://adventofcode.com/2018

## How to run

```
$ ruby aoc.rb [PROBLEM NUMBER] [OPTION]
```

Options:

`-x` or `--example` to run the problem with the example input instead of the real input.