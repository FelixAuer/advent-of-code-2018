require File.expand_path(File.dirname(__FILE__) + '/solution')

class Solution05 < Solution
  def part_one
    chemicals = @input[0]
    react(chemicals)
  end

  def part_two
    chemicals = @input[0]
    ("a"..."z").reduce(chemicals.length) { |current_min, character| [current_min, react(chemicals.gsub(/#{character}/i, ''))].min }
  end

  private

  def react(chemicals)
    pointer = 0
    until pointer == chemicals.length - 1
      if chemicals[pointer] == chemicals[pointer + 1].swapcase
        chemicals[pointer + 1] = ""
        chemicals[pointer] = ""
        pointer -= 1
        pointer = 0 if pointer < 0
      else
        pointer += 1
      end
    end
    chemicals.length
  end
end
