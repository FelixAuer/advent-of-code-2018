require File.expand_path(File.dirname(__FILE__) + '/solution')

class Solution02 < Solution
  def part_one
    counts = []
    @input.each { |row| counts << row.reduce(Hash.new(0)) { |hash, element| hash[element] += 1; hash } }
    contains_two = counts.select { |char_count| char_count.values.include?(2) }
    contains_three = counts.select { |char_count| char_count.values.include?(3) }
    contains_two.count * contains_three.count
  end

  def part_two
    @input.combination(2).each do |combo|
      diff = combo[0].select.each_with_index { |item, index|
        combo[1][index] != item
      }.count
      if diff == 1
        return combo[0].select.each_with_index { |item, index|
          combo[1][index] == item
        }.join("")
      end
    end
  end

  def transform_input_line(line)
    line.strip.split("")
  end
end
