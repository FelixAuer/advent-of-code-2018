class Solution

  def initialize(example_mode = false)
    load_file(example_mode)
  end

  def load_file(example_mode = false)
    @input = []
    problem_number = self.class.to_s[/\d+/]
    file_name = example_mode ? 'example.txt' : 'input.txt'
    open(File.expand_path(File.dirname(__FILE__) + "/../input/#{problem_number}/#{file_name}"), &method(:parse_file))
  end

  def parse_file(file)
    while (line = file.gets)
      @input << transform_input_line(line)
    end
    before_solving
  end

  def before_solving; end

  def transform_input_line(line)
    line
  end

  def part_one
    "to be implemented..."
  end

  def part_two
    "to be implemented..."
  end

end