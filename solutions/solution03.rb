require File.expand_path(File.dirname(__FILE__) + '/solution')

class Solution03 < Solution
  def part_one
    @fabric = Hash.new { |hash, key| hash[key] = Hash.new(0) }
    @input.each do |claim|
      claim.range_x.each do |pos_x|
        claim.range_y.each do |pos_y|
          @fabric[pos_x][pos_y] += 1
        end
      end
    end
    sum = 0
    @fabric.each do |x, row|
      row.each do |y, count|
        sum += 1 if count > 1
      end
    end
    sum
  end

  def part_two
    @input.each do |claim|
      unique = true
      claim.range_x.each do |pos_x|
        claim.range_y.each do |pos_y|
          unique = false if @fabric[pos_x][pos_y] > 1
        end
      end
      return claim.id if unique
    end
  end

  def transform_input_line(line)
    id, pos_x, pos_y, width, height = line.match(/#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/).captures
    Claim.new(id, pos_x, pos_y, width, height)
  end
end

class Claim
  attr_reader :id
  attr_reader :pos_x
  attr_reader :pos_y
  attr_reader :width
  attr_reader :height

  def initialize(id, pos_x, pos_y, width, height)
    @id = id
    @pos_x = pos_x.to_i
    @pos_y = pos_y.to_i
    @width = width.to_i
    @height = height.to_i
  end

  def range_x
    @pos_x...(@pos_x + @width)
  end

  def range_y
    @pos_y...(@pos_y + @height)
  end

  def inspect
    "ID: #{id} X: #{@pos_x}, Y: #{@pos_y}, width: #{@width}, height: #{@height},"
  end
end

def ranges_intersect?(range1, range2)
  range1.begin <= range2.end && range2.begin <= range1.end
end
