require File.expand_path(File.dirname(__FILE__) + '/solution')

class Solution01 < Solution
  def part_one
    @input.reduce(0) { |value, element| value + element }
  end

  def part_two
    frequency = 0
    frequencies = {}
    @input.cycle do |change|
      frequency += change
      return frequency if frequencies[frequency] == 1
      frequencies[frequency] = 1
    end
  end

  def transform_input_line(line)
    line.to_i
  end
end
