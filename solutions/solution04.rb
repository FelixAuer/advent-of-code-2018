require File.expand_path(File.dirname(__FILE__) + '/solution')

class Solution04 < Solution
  def create_histo
    @histo = Hash.new { |h, k| h[k] = Hash.new(0) }
    guard = 0
    minute = 0
    @input.each do |line|
      if (id = line[/ Guard #(\d+) begins shift/, 1]) != nil
        guard = id
      end
      if (minute_log = line[/:(\d+)] falls asleep/, 1]) != nil
        minute = minute_log
      end
      if (minute_log = line[/:(\d+)] wakes up/, 1]) != nil
        (minute...minute_log).each do |index|
          @histo[guard][index] += 1
        end
        minute = minute_log
      end
    end
  end

  def part_one
    max_sleep = 0
    sleepiest_guard = 0
    @histo.each do |guard_number, guard_stats|
      sleeptime = guard_stats.values.reduce(:+)
      if sleeptime > max_sleep
        max_sleep = sleeptime
        sleepiest_guard = guard_number
      end
    end
    sleepiest_minute = @histo[sleepiest_guard].max_by { |k, v| v }[0]
    solution = sleepiest_guard.to_i * sleepiest_minute.to_i
    "Guard ##{sleepiest_guard} at minute #{sleepiest_minute}: #{solution}"
  end

  def part_two

    max_sleep = 0
    sleepiest_guard = 0
    sleepiest_minute = 0
    @histo.each do |guard_number, guard_stats|
      sleepiest_minute_guard = guard_stats.max_by { |k, v| v }
      if sleepiest_minute_guard[1] > max_sleep
        max_sleep = sleepiest_minute_guard[1]
        sleepiest_minute = sleepiest_minute_guard[0]
        sleepiest_guard = guard_number
      end
    end

    solution = sleepiest_guard.to_i * sleepiest_minute.to_i
    "Guard ##{sleepiest_guard} at minute #{sleepiest_minute}: #{solution}"
  end

  def before_solving
    @input.sort!
    create_histo
  end
end
