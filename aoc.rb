problem = ARGV[0].rjust(2, "04")
example_mode = %w[-x --example].include?(ARGV[1])

require File.expand_path(File.dirname(__FILE__) + '/solutions/solution' + problem)

classname = "Solution#{problem}"
class_const = Object.const_get(classname)
solution = class_const.new(example_mode)

puts "--- Problem #{problem} ---"
puts "--- EXAMPLE ---" if example_mode
print "Part 1: "
puts solution.part_one
print "Part 2: "
puts solution.part_two